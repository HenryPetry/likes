<?php

declare(strict_types = 1);

use code\kata\likes\meLikey;

require __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;

final class likesTest extends TestCase
{
    public $meLikey;

    public function setUp(): void
    {
        $this->meLikey = new meLikey();
    }

    /**
     * @dataProvider peopleProvider
     */
    public function testValidLikes(array $people, string $expected): void
    {
        $actual = $this->meLikey->likes($people);

        $this->assertEquals($expected, $actual);
    }

    public function peopleProvider(): array
    {
        return [
            [[], 'no one likes this'],
            [['Peter'], 'Peter likes this'],
            [['Jacob', 'Alex'], 'Jacob and Alex like this'],
            [['Max', 'John', 'Mark'], 'Max, John and Mark like this'],
            [['Alex', 'Jacob', 'Mark', 'Max'], 'Alex, Jacob and 2 others like this'],
            [['Ruby', 'Pearl', 'Matthew', 'Mark', 'Luke', 'John', 'Peter', 'Alex', 'Jacob', 'Max', 'Xena', 'Zelda'], 'Ruby, Pearl and 10 others like this'],
        ];
    }
}
