# Code Kata : Likes #

Implements a function to show “likes” of a post (like on Facebook and what not).

### Installation ###
- `git clone git@bitbucket.org:HenryPetry/likes.git`
- `cd likes`
- `composer install`

### Running CLI samples ###

 `php ./src/likes.php`

### Running Unit tests ###

`./vendor/bin/phpunit tests`