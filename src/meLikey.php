<?php
/**
 * Code Kata
 * Implement a function to show “likes” of a post (like on Facebook and what not).
 */

declare(strict_types = 1);

namespace code\kata\likes;

class meLikey
{
    const RESPONSES = [
        1 => '%s likes this',
        2 => '%s and %s like this',
        3 => '%s, %s and %s like this',
        4 => '%s, %s and %d others like this',
    ];

    public function likes(array $people = []): string
    {
        $peopleCount = min(count($people), count(self::RESPONSES));

        if (array_key_exists($peopleCount, self::RESPONSES)) {
            if ($peopleCount == count(self::RESPONSES)) {
                $people = $this->truncatePeople($people);
            }

            return vsprintf(self::RESPONSES[$peopleCount], $people);
        }

        return 'no one likes this';
    }

    private function truncatePeople(array $people): array
    {
        $displayNames = 2;
        $othersCount = count($people) - $displayNames;
        $peopleGroup = array_chunk($people, $displayNames);

        return array_merge($peopleGroup[0], [$othersCount]);
    }
}
