<?php

declare(strict_types = 1);

require __DIR__ . '/../vendor/autoload.php';

use code\kata\likes\meLikey;

$meLikey = new meLikey();

// must be "no one likes this"
echo $meLikey->likes([]) . "\n";

// must be "Peter likes this"
echo $meLikey->likes(['Peter']) . "\n";

// must be "Jacob and Alex like this"
echo $meLikey->likes(['Jacob', 'Alex']) . "\n";

// must be "Max, John and Mark like this"
echo $meLikey->likes(['Max', 'John', 'Mark']) . "\n";

// must be "Alex, Jacob and 2 others like this"
echo $meLikey->likes(['Alex', 'Jacob', 'Mark', 'Max']) . "\n";

// must be "Ruby, Pearl and 10 others like this"
echo $meLikey->likes(['Ruby', 'Pearl', 'Matthew', 'Mark', 'Luke', 'John', 'Peter', 'Alex', 'Jacob', 'Max', 'Xena', 'Zelda']) . "\n";
